class HomeController < ApplicationController
  def index
  	@test_data = SmarterCSV.process('test_data_sample.csv')

  	respond_to do |format|
      format.html { render template: "home/index.html.erb"}
      format.json { render :json => @test_data }
    end 
  end
end
